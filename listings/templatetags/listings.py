from django import template

register = template.Library()
@register.inclusion_tag("renderers/listing_summary.html")
def listing_summary(listing):
    return {'listing': listing}