from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils.timezone import now

from listings.models import Listing, MLSZone, Rating
from listings.tasks import rescan_zones, rescan_latest_zones 


def index(request):
    latest_zones = MLSZone.objects.order_by('-id')[:5]
    latest_listings = Listing.objects.order_by('-id')[:20]

    ctx = {
        'zones': latest_zones,
        'listings': latest_listings
    }
    return render(request, 'index.html', ctx)


def property_index(request):
    listings = Listing.objects.all()

    tpl = loader.get_template('listings/main.html')
    ctx = Context({
        'listings': listings
    })
    return HttpResponse(tpl.render(ctx))

def start_zone_scanning(request):
    rescan_zones.delay()

def zone_list(request):
    zones = MLSZone.objects.all()

    return render(request, 'zones/index.html', {'zones': zones})

def zone_refresh(request, id):
    rescan_latest_zones.delay(id)

    return redirect('zone_list')

def zone_detail(request, id):
    zone = MLSZone.objects.get(id=id)
    latest_listings = zone.listings.order_by('-date_last_updated')[:20]

    return render(request, 'zones/detail.html', {'zone': zone, 'listings': latest_listings})



def listing_accept(request, id):
    try:
        rating = Rating.objects.get(listing_id=id)
    except Rating.DoesNotExist:
        rating = Rating(rated_on = now(), listing_id=id)

    rating.rejected = False
    rating.saved = True

    rating.save()

    return HttpResponse(status=204)


def listing_reject(request, id):
    try:
        rating = Rating.objects.get(listing_id=id)
    except Rating.DoesNotExist:
        rating = Rating(rated_on = now(), listing_id=id)

    rating.rejected = True
    rating.saved = False
    rating.rated_on = now()

    rating.save()

    return HttpResponse(status=204)

def new_zone(request):
    if request.method == 'POST':
        zone = MLSZone()
        zone.name = request.POST['name']
        zone.url = request.POST['url']

        zone.save()

        ctx = {
            'message': 'The zone was successfuly created [id=%s]' % (zone.id, ) 
        }
    else:
        ctx = {}

    return render(request, 'zones/new.html', ctx)





