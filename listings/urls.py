from django.conf.urls import patterns, url
from listings import views

urlpatterns = patterns('',
					   url(r'^$', views.index, name="index"),
					   url(r'^zones/$', views.zone_list, name="zone_list"),
					   url(r'^zones/(?P<id>\d+)/$', views.zone_detail, name="zone_detail"),
					   url(r'^zones/(?P<id>\d+)/refresh$', views.zone_refresh, name="zone_refresh"),
					   url(r'^zones/new$', views.new_zone, name="zone_add"),
					   url(r'^listings/(?P<id>\d+)/accept$', views.listing_accept, name="listing_accept"),
					   url(r'^listings/(?P<id>\d+)/reject$', views.listing_reject, name="listing_reject"),
                       url(r'^properties/$', views.property_index, name="property_index"),
                       )
