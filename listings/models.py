from django.db import models
from urllib import unquote, quote
import demjson
from datetime import date, timedelta


class Listing(models.Model):
    url = models.URLField(max_length=2000, blank=False)
    scraped_data = models.TextField()
    date_scraped = models.DateTimeField()
    date_last_updated = models.DateTimeField()
    price = models.FloatField()
    latitude = models.FloatField()
    longitude = models.FloatField()
    address = models.CharField(max_length=500)
    mls_number = models.CharField(max_length=100)

    def property_json(self):
        if hasattr(self, 'fixed'):
            return self.fixed

        self.fixed =  demjson.decode(self.scraped_data.replace("u'", "'").replace("True", 'true'))
        
        return self.fixed


    def __unicode__(self):
        return "%s (%s)" % (self.address, self.price)

class MLSZone(models.Model):
    name = models.CharField(max_length=200)
    url = models.CharField(max_length=4096)
    listings = models.ManyToManyField(Listing)

    @staticmethod
    def from_url(name, url):
        zone = MLSZone()
        zone.name = name
        zone.url = url
        
        return zone

    def queryurl(self):
        url, params = self.url.split('?', 1)
        decoded = unquote(params)

        start = decoded.index('<ListingStartDate>')
        end = decoded.index('</ListingStartDate>')
        days = (date.today() - timedelta(days=7))

        new_node = "<ListingStartDate>%s</ListingStartDate>" % (days.strftime('%d/%m/%Y'), )
        if start and end:
            corrected = decoded[:start] + new_node + decoded[end:]
        else:
            cidx = decoded.index("</Culture>") + 10

            corrected = decoded[:cidx] + new_node + decoded[cidx:]

        return '%s?%s' % (url, quote(corrected))

class Rating(models.Model):
    listing = models.ForeignKey(Listing, unique=True)
    saved = models.BooleanField()
    rejected = models.BooleanField()
    notes = models.CharField(max_length=5000)
    rated_on = models.DateTimeField()