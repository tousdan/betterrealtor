from celery.task import task
from listings.models import MLSZone, Listing
from django.utils.timezone import now

import requests
import logging
import json

REALTOR_URL = "http://www.realtor.ca/handlers/MapSearchHandler.ashx?"

logger = logging.getLogger(__name__)


@task
def rescan_latest_zones(zone_id):
    zone = MLSZone.objects.get(pk=zone_id)

    logger.info('Scanning zone id=%s' % (zone.id))
    logger.info('URL: %s' % (zone.url))
    logger.info('Corrected: %s' % (zone.queryurl()))
    logger.info('URL: %s' % (zone.url))
    r = requests.get(zone.url)

    # Annoying realtor.ca produces invalid json, correct it.
    payload = json.loads(r.content.replace("\\'", ""))
    result_count, results = payload['NumberSearchResults'], payload['MapSearchResults']

    for result in results:
        process_property.delay(zone.id, result)


@task
def rescan_zones():
    zones = MLSZone.objects.all()

    for zone in zones:
        rescan_latest_zones.delay(zone.id)


@task
def process_property(zone_id, property):
    mls = property['MLS']

    try:
        existing = Listing.objects.filter(mls_number=mls)[0]
    except IndexError:
        existing = None

    zone = MLSZone.objects.get(pk=zone_id)

    if not existing:
        scrape_date = now()
        listing = Listing()

        listing.mls_number = mls
        listing.scraped_data = property
        listing.date_scraped = scrape_date
        listing.date_last_updated = scrape_date
        listing.url = property['DetailsLink']
        listing.price = float(property['Price'].replace('$', '').replace(',', ''))
        listing.latitude = float(property['Latitude'])
        listing.longitude = float(property['Longitude'])
        listing.address = property['Address']

        listing.save()
        
        zone.listings.add(listing)

        

        logger.info('Saved new listing %s (MLS: %s)' % (listing.id, mls))
    else:
        logger.info('Found a match for this mls number %s (MLS: %s)' % (existing.id, mls))
        # is the listing already in the current zone?
        for linked_zone in existing.mlszone_set.all():
            if linked_zone.id == zone.id:
                logger.info('Property is already linked to this zone')
                return

        logger.info('Linking existing property to zone')
        zone.listings.add(existing)
